import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Register extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterState();
  }
}

class RegisterState extends State<Register> {
  String _name;
  String _email;
  String _gender;
  String _designation;
  String _phoneNumber;
  String _station;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void createRecord(){
    Firestore.instance.collection('users').document(_phoneNumber).setData({
        'name': _name,
        'email': _email,
        'gender': _gender,
        'designation': _designation,
        'station': _station

    });
    Fluttertoast.showToast(
      msg: 'Registered Succesfully',
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 5,
      backgroundColor: Colors.green,
      textColor: Colors.white,
      fontSize: 16.0
    );

  }

  Future storeIndb() async{
    final snapshot=await Firestore.instance.collection('users').document(_phoneNumber).get();
    if(snapshot==null || !snapshot.exists){
      print('doesnt exist');
      createRecord();
    }
    else{
      Fluttertoast.showToast(
        msg: 'It seems you are already a user, try login',
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 5,
        backgroundColor: Colors.red,
        fontSize: 15.0,
        textColor: Colors.white
      );
    }
  }

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Name'),
      maxLength: 10,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Name is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _name = value;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Email'),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Email is Required';
        }

        if (!RegExp(
                r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
            .hasMatch(value)) {
          return 'Please enter a valid email Address';
        }

        return null;
      },
      onSaved: (String value) {
        _email = value;
      },
    );
  }

  Widget _buildGender() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Gender'),
      keyboardType: TextInputType.visiblePassword,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Gender is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _gender = value;
      },
    );
  }

  Widget _builStation() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Station'),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Station is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _station = value;
      },
    );
  }

  Widget _buildPhoneNumber() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Phone number'),
      keyboardType: TextInputType.phone,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Phone number is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _phoneNumber = '+91'+value;
      },
    );
  }

  Widget _buildDesignation() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Designation'),
      validator: (String value) {

        if (value.isEmpty) {
          return 'Designation is required';
        }

        return null;
      },
      onSaved: (String value) {
        _designation = value;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView( child:
      Container(
        margin: EdgeInsets.all(24),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildName(),
              _buildEmail(),
              _buildGender(),
              _buildDesignation(),
              _buildPhoneNumber(),
              _builStation(),
              SizedBox(height: 60),
              RaisedButton(
                child: Text(
                  'Submit',
                  style: TextStyle(color: Colors.blue, fontSize: 16),
                ),
                onPressed: () {
                  if (!_formKey.currentState.validate()) {
                    return;
                  }

                  _formKey.currentState.save();
                  storeIndb();

                  print(_name);
                  print(_email);
                  print(_phoneNumber);
                  print(_station);
                  print(_designation);
                  print(_gender);
                  //Send to API
                },
              )
            ],
          ),
        ),
      ),
        ),
      ),
    );
  }
}
