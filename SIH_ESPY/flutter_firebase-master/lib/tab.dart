import 'package:flutter/material.dart';
import 'package:flutter_firebase/firebase/auth/phone_auth/get_phone.dart';
import 'package:flutter_firebase/register.dart';

class TabUser extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: <Widget>[
              Tab(text: 'Log In',),
              Tab(text: 'Sign Up',)
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            PhoneAuthGetPhone(),
            Register()
          ],
        )
      ),
    );
  }
}